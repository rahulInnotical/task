/**
 	* first install the async package in your code.(npm install async)
 	* after install require the async package.
 	* async.forEach runs through the array in parallel,
 	* meaning it will run the function for each item in the array immediately,
 	* And then when all of them execute the callback, the callback function will be called.
 	* using node version v7.10.0
*/

const async = require('async');

var arrayData = [1,2,3,4,5,6,7,8,9,10,11, 12,,,,,,,,,,100];

async.forEachSeries(arrayData,(data, callBack)=>{
	if(data){
	    setTimeout(function() {
	        console.log(data);
	        callBack(); 
	    }, 1000);
		
	}
},(err)=>{
	if(err){
		console.log("err", err);
	}
	else{
		console.log('successfully Iterated loop');
	}

})
	