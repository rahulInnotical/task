#include<stdio.h>
void function_tree(int n); //enter value of n which you want to print.

int main(void) {
    function_tree(6);
    
}
void function_tree(int n){
    int columnLength,evenOddCheck,i,j,oddcount=1,evencount=1,spaceStart,lastSpace,totalSpace,count=0;
    evenOddCheck=n/2;
     columnLength=(evenOddCheck*4)+3;
    int mid= columnLength/2;
    for(i=0;i<n;i++){
        for(j=0;j< columnLength;j++){
            if(i%2!=0){
                totalSpace=  columnLength-(4*evencount+3);
                spaceStart=totalSpace/2;
                lastSpace= columnLength-spaceStart;
                if((j>=0&&j<spaceStart)||(j>=lastSpace && j< columnLength)){
                    printf("_");
                }else if(j>=spaceStart && j<lastSpace){
                    if(count==evencount){
                        count=0;
                        printf("_");
                    }else{
                        count++;
                        printf("*");
                    }
                }
            }else{
                if(j>(mid-oddcount)&& j<(mid+oddcount)){
                    printf("*");
                }else{
                    printf("_");
                }
            }
        }            
        if(i%2!=0)
            evencount++;
        else
            oddcount++;
        count=0;
        printf("\n");
    }
    
}